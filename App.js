import * as React from 'react';
// asdassasfsdfds
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from "./screens/home"
// import profile from "./screens/profile"
import verify from "./screens/verification"
import otpVerification from "./screens/otpVerification"
import service from "./screens/service";
import serviceProviderList from "./screens/serviceprovider";
import providerdetails from "./screens/providerdetail";
import Quatation from "./screens/cotation";
import Addtowallet from "./screens/addToWallet";
import BookingDone from "./screens/payforbooking";
import jobdisc from "./screens/jobstatus";
import booking from "./screens/bookingconfirm";
import cancelbookingreason from "./screens/cancelreason";
import visitpendingpage from "./screens/visitpending";
import AcceptQuatation from "./screens/acceptquatation";
import WorkingDone from "./screens/workdone";
import ReviewScreen from "./screens/reviewscreen";
import BlankScreen from "./screens/extrascreen";
import RegistrationScreen from "./screens/providerRegistration"
import LoginScreen  from "./screens/loginScreen"


const Stack = createNativeStackNavigator();
// const Drawer = createDrawerNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* done */}
      <Stack.Screen
          name="Home"
          component={Home}
          options={{ title: 'Welcome' }}
        />
        {/* done */}
        
        <Stack.Screen
          name="loginScreen"
          component={ LoginScreen }
          options={{ title: 'Login' }}
        />
      <Stack.Screen
          name="Registeration"
          component={RegistrationScreen}
          options={{ title: 'Register' }}
        />
        <Stack.Screen
          name="service"
          component={service}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen 
          name="verify" 
          component={verify}/>
        <Stack.Screen 
          name="otpScreen" 
          component={otpVerification}/>
        <Stack.Screen 
          name="serviceProviderList" 
          component={ serviceProviderList }/>
        <Stack.Screen 
          name="prodetail" 
          component={providerdetails}/>
        <Stack.Screen 
          name="cotation" 
          component={Quatation}/>
        <Stack.Screen 
          name="walletpay" 
          component={Addtowallet}/>
        <Stack.Screen 
          name="walletcancel" 
          component={Addtowallet}/>
        <Stack.Screen 
          name="bookok" 
          component={BookingDone}/>
        <Stack.Screen 
          name="bookcancel" 
          component={BookingDone}/>
        <Stack.Screen 
          name="booked" 
          component={jobdisc}/>
        <Stack.Screen 
          name="confirmok" 
          component={booking}/>
        <Stack.Screen 
          name="confirmnot" 
          component={booking}/>
        <Stack.Screen 
          name="cancelreason" 
          component={cancelbookingreason}/>
        <Stack.Screen 
          name="visitpending" 
          component={visitpendingpage}/>
        <Stack.Screen 
          name="accept" 
          component={AcceptQuatation}/>
        <Stack.Screen 
          name="workdone" 
          component={WorkingDone}/>
        <Stack.Screen 
          name="reviewscreen" 
          component={ReviewScreen}/>
        <Stack.Screen 
          name="new" 
          component={BlankScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

{/* <Drawer.Navigator initialRouteName="Home">
<Drawer.Screen name="Home" component={Home} />
<Drawer.Screen name="Notifications" component={otpVerification} />
</Drawer.Navigator> */}
