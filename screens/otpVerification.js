import * as React from 'react';
import { View , Image , Text , TouchableOpacity  , StyleSheet, TextInput} from 'react-native';
import { resendUserOtp } from "../logics"
const otpVerificationScreen = ({ navigation }) => {        
    //otp resend function
    const ResendOTP = () => {
        // Alert.alert('Button Pressed!');
        console.log(" otp resend function");
        resendUserOtp()
        .then((result)=>{
            //services data 
            console.log("otp has sended");
            Alert.alert(
                "Massage",
                "OTP has been sent.",
                [
                  {
                    text: "OK",
                    // onPress: () => Alert.alert("OK Pressed"),
                    // style: "",
                  },
                ],
                // {
                //   cancelable: true,
                //   onDismiss: () =>
                //     Alert.alert(
                //       "This alert was dismissed by tapping outside of the alert dialog."
                //     ),
                // }
              )
            console.log(result.data);
            // setServices(result.data.result)
        })
        .catch((error)=>{
            Alert.alert(
                "Warning",
                "Something went wrong - api failure",
                [
                  {
                    text: "OK",
                    // onPress: () => Alert.alert("OK Pressed"),
                    // style: "",
                  },
                ],
                // {
                //   cancelable: true,
                //   onDismiss: () =>
                //     Alert.alert(
                //       "This alert was dismissed by tapping outside of the alert dialog."
                //     ),
                // }
              )
        })

        // navigation.navigate("verify");
    }


    const VerifyOTP = () => {
        // Alert.alert('Button Pressed!');
        otpVerificationFailed()
        console.log("navigate to service home screen");
        // navigation.navigate("service");
    }

    const otpVerificationFailed = ()=>{
        Alert.alert([
            "Warning",
            "unable to verify",
            [
                {
                    text: "Resend",
                    onPress: () => VerifyOTP() ,
                    // style: "",
                },
                {
                    text: "OK",
                    // onPress: () => Alert.alert("OK Pressed"),
                    // style: "",
                }
            ]
        ])
    }


        return (
            <View style={styles.container}>
                <View style={styles.backgroundContainer}>
                    <Text>Verification</Text>
                    <Text>Enter OTP</Text>
                    <TextInput type="text" style={styles.inputField} placeholder="Enter your otp"/>
                    <Button onPress={ ResendOTP }  style={styles.loginButton} title="Resend OTP" />
                    <Button onPress={ VerifyOTP }  style={styles.loginButton} title="Verify" />
                </View>
            </View>
        );
    }


export default otpVerificationScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: "3%", 
        height: "3%"
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    }
});
