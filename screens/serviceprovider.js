import * as React from 'react';
import { ScrollView , View , Image , Text , TouchableOpacity  , StyleSheet, TextInput} from 'react-native';
    
const serviceProviderListScreen = ({ navigation }) => {        
    // const ResendOTP = () => {
    //     // Alert.alert('Button Pressed!');
    //     console.log(" otp resend function");
    //     // navigation.navigate("verify");
    // }

    const toServiceProviderScreen = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to service provider list screen");
        navigation.navigate("serviceProviderList");
    }

    var SampleNameArray = [ "Pankaj", "Rita", "Mohan", "Amit", "Babulal", "Sakshi" ,"Pankaj", "Rita", "Mohan", "Amit", "Babulal", "Sakshi" ];
            return (
                <View>
                <ScrollView>
                        <View>
                            <TextInput type="text" style={styles.inputField} placeholder="search...."/>
                            {
                             SampleNameArray.map((item, key)=>(
                                <TouchableOpacity key={key} onPress={ toServiceProviderScreen } >
                                <Image style={styles.creausalImage} source={require('../assets/broken-cable.png')} />                        
                                <View style={styles.loginButton}>
                                    <Text>{ item }</Text>
                                </View>
                                </TouchableOpacity>
                             ))
                             }
                             </View>     
                             <TouchableOpacity style={styles.nowButton} onPress={ toServiceProviderScreen } >
                        <Text>Book Now</Text>
                </TouchableOpacity>
                </ScrollView>

            </View>  
        );
    }


export default serviceProviderListScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    creausalImage: {
        flex: 1, 
        width: 10, 
        height: 40
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    },
    nowButton : 
    {
        borderWidth : 2,
        backgroundColor : "pink"
    }
});
