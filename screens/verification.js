import * as React from 'react';
import { View , TextInput , Image , Text , TouchableOpacity  , StyleSheet} from 'react-native';
    
const OtpVerificationScreen = ({ navigation }) => {        
    const onPressHandler = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the otp verification screen");
        navigation.navigate("otpScreen");
    } 
        return (
            <View style={styles.container}>
                <View style={styles.backgroundContainer}>
                    <Text>Verification</Text>
                    <Text>Enter Your Mobile Number</Text>
                    <TextInput type="text" style={styles.inputField} placeholder="Enter your Mobile Number"/>
                    <Text>We will send OTP on your Number</Text>
                    <TouchableOpacity onPress={ onPressHandler } >
                        <View style={styles.loginButton}>
                            <Image style={styles.bakcgroundImage} source={require('../assets/init_background.png')} />
                            <Text>send OTP</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }


export default OtpVerificationScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: "3%", 
        height: "3%"
    },
    loginButton: {
        marginBottom: 40
    },
    loginButton : {
        // flex : 1,
        width : 100,
        height: 50,
        backgroundColor : "red",
        justifyContent : "center",
        textAlign : "center"
    },
    inputField : 
    {
        borderColor : "pink",
        borderWidth : 2,
        // backgroundColor : "black",
        color : "red",
    }
});
