import * as React from 'react';
import { View , Image , Text , TouchableOpacity  , StyleSheet} from 'react-native';
    
const HomeScreen = ({ navigation }) => {        
    const onPressHandler = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the otp verification screen");
        navigation.navigate("Registeration");
    } 
        return (
            <View style={styles.container}>
                <View style={styles.backgroundContainer}>
                <Image style={styles.bakcgroundImage} source={require('../assets/init_background.png')} />
                    <TouchableOpacity onPress={ onPressHandler } >
                        <Text>click me</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }


export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    loginButton: {
        marginBottom: 40
    }
});
