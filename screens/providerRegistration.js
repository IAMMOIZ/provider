// import * as React from 'react';
import React, {useState , useEffect} from 'react';
import {  SafeAreaView , TextInput , Button , View , Image , Text , TouchableOpacity  , StyleSheet, ImageBackground, ScrollView} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import axios from 'axios';
import { userSignup , getServices } from "../logics"


const ProviderRegistrationScreen = ({ navigation }) => {
    
    // all states
    let [ name , setName ]  = useState("");
    let [ email , setEmail ]  = useState("");
    let [ password , setPassword ]  = useState("");
    let [ number , setNumber ]  = useState("");
    let [ address , setAddress ]  = useState("");
    let [ services , setServices] = useState([]);

    //event handler function
    const onPressHandler = () => {
        // Alert.alert('Button Pressed!');
        console.log("navigate to the otp verification screen");
        // axios.get('https://homefixit.in/getServiceCategories')
        //     .then(function (response) {
        //         console.log("success",response.data , response.status);
        //     })
        //     .catch(function (error) {
        //         console.log("error" ,error);
        //     });
        // navigation.navigate("verify");
    } 

    const submitData = ()=>{
        console.log(name , address , number , password ,email );
        userSignup( {
            name : name ,
            email :email,
            contact :number,
            password : password,
            lat :"12.73232",
            lon :"21.23423",
            address: address ,
            device_type:"android",
            device_token :""
          } )
          .then(( result ) => {
              console.log("result" , result.result);
              navigation.navigate("verify");
          })
          .catch(( error )=>{
              console.log("error",error);
          })
    }


    const goToLogin = ()=>{
        navigation.navigate("loginScreen");
    }
    //effect handling and api calling    
    useEffect(()=>{
        console.log("hiiiii");
        getServices()
        .then((result)=>{
            //services data 
            console.log(result.data);
            // setServices(result.data.result)
        })
        .catch((error)=>{
            Alert.alert(
                "Warning",
                "Something went wrong - api failure",
                [
                  {
                    text: "OK",
                    // onPress: () => Alert.alert("OK Pressed"),
                    // style: "",
                  },
                ],
                // {
                //   cancelable: true,
                //   onDismiss: () =>
                //     Alert.alert(
                //       "This alert was dismissed by tapping outside of the alert dialog."
                //     ),
                // }
              )
        })
        // setName(response.data.name)
    } , [ name ])


        return (
            <ScrollView>
            <ImageBackground source={require('../assets/otp-bg.png')} style={styles.bakcgroundImage}>
            <SafeAreaView style={styles.container}>
            <Image source={require('../assets/slider.png')} style={styles.image} />
                      <TextInput
                    style={styles.input}
                    value={ name }
                    placeholder="Enter Your Name"
                    onChangeText = { setName }
                />

                <TextInput
                    style={styles.input}
                    value={ email }
                    placeholder="Enter Your Email"
                    onChangeText = { setEmail }
                />

                <TextInput
                    style={styles.input}
                    value={ password }
                    secureTextEntry={true}
                    placeholder="Enter Your password"
                    onChangeText = { setPassword }
                />

                <TextInput
                    style={styles.input}
                    value={ number }
                    placeholder="Enter Your Number"
                    keyboardType="numeric"
                    maxLength={10}
                    onChangeText = { setNumber }
                /> 

                <TextInput
                    style={styles.input}
                    value={ address }
                    placeholder="Enter Your Address"
                    multiline
                    numberOfLines={5}
                    onChangeText = { setAddress }
                />

                <Picker style={styles.input} 
                    onValueChange={currentService => setServices( currentService )}>
                    { services.map((service)=>
                    {
                        <Picker.Item label={service.service_name} value={service.service_name} />
                    })}
                </Picker>
                <Button title="Registser"  onPress={ submitData }/>
                <Text>-----OR-----</Text>
                <Button title="Login"  onPress = { goToLogin }/>
            
            </SafeAreaView>
            </ImageBackground>
            </ScrollView> 
            );
    }


export default ProviderRegistrationScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        marginTop: 0,
    },
    backgroundContainer: {
        flex: 1,
        position: 'absolute',
        top:0,
        left: 0,
        width: '100%',
        height: '100%',
    }, 
    bakcgroundImage: {
        flex: 1, 
        width: null, 
        height: null
    },
    loginButton: {
        width:'30%',
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal: 135,
        marginTop: 20,
        borderRadius: 30,
    },
    input : {
        marginTop : 10,
        borderColor : "#48dbfb",
        borderWidth :5,
        height: 40,
        padding: 10,
        backgroundColor: '#7ed6df',
        borderRadius: 30,
        marginHorizontal :7,
    },
    // btn:{
    //     width:'30%',
    //     justifyContent:'center',
    //     alignItems:'center',
    //     marginHorizontal: 135,
    //     marginTop: 20,
    //     borderRadius: 30,
    // },
    image:{
        marginHorizontal: 100,
        width: 200,
        height: 200,
        alignItems: 'center',
        justifyContent:'center'
    }
});

