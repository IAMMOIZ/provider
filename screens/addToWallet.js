import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const walletpay=()=>{
    console.log("pay success")
    navigation.navigate("walletpay")
}

const walletcancel=()=>{
    console.log("pay cancel")
    navigation.navigate("walletcancel")
}

export default function App7({ navigation }) {
        return(
            <View>
                <Text>Please Add payment Account to Wallet </Text>
                <Button title="Pay" onPress={walletpay}/>
                <Button title="Cancel" onPress={walletcancel}/>
            </View>
        )    
}
const styles = StyleSheet.create({
    page7 :{
        fontSize : 30
    }
})