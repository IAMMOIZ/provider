import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const bookingpay=()=>{
    console.log("pay to ok");
    navigation.navigate("bookok");
}

const bookingcancel=()=>{
    console.log("pay to cancel");
    navigation.navigate("bookcancel");
}

export default function App8({ navigation }) {
    return(
        <View>
            <Text style={styles.app8}>click to ok button confirm booking</Text>
            <Button title="Confirm Booking" onPress={bookingpay}/>
            <Button title="Cancel Booking" onPress={bookingcancel}/>
        </View>
    );
}

const styles=StyleSheet.create({
    app8 :{
        color : "red"
    }
})