import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const newscreen=()=>{
    console.log("ok");
    navigation.navigate("new");
}

export default function App15({ navigation }) {
    return(
        <View>
            <Text style={styles.page15}>this is New screen</Text>
            <Button title="Done" onPress={newscreen}/>
        </View>
    );
}

const styles=StyleSheet.create({
    page15 :{
        color : "red"
    }
})