import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const providerdetail=()=>{
    console.log("Service Provider Detail")
    navigation.navigate("prodetail")
}

export default function App5({ navigation }) {
        return(
            <View>
                <Text style={styles.page5}>this is Service Provider Details</Text>
                <Button title="Book Now" onPress={providerdetail}/>
            </View>
        )    
}

const styles=StyleSheet.create({
    page5 :{
        fontSize : 30
    }
})